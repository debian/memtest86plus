# README for Debian package maintainers and contributors

This Debian packaging source code in directory `debian/` is maintained on branch
`debian/master` (naming following DEP-14).

For help to manage the Git workflow is used [gbp](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.patches.html) (formerly git-buildpackage).

Is preconfigured with `debian/gbp.conf` so the gbp commands don't need extra
parameters most of the time.

For managing Debian patches can be used [gbp pq](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.patches.html) (recommended) or [Quilt](https://wiki.debian.org/UsingQuilt).

The use of `gbp pq` makes it easy to cherry-pick commits between upstream
and downstream Debian.

`gbp pq` creates a "patch branch", when you want to directly edit upstream
source files for export to quilt patches, however this is for local use only
and should not be pushed to remote servers.

## Getting the Debian packaging source code

To get the Debian packaging source code and have the upstream remote alongside
it, simply run:

    gbp clone git@salsa.debian.org:debian/memtest86plus.git

To build the package one needs all three Debian branches (`debian/master`,
`upstream/latest`and `pristine-tar`). Using `gbp clone` and `gbp pull` ensures
all three branches are automatically fetched and updated.

